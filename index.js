const express = require('express');
const app = express();
const server = require('http').Server(app);
const port = 3000;

const data = [
{"id": 1, "name": "Sweden", "population": 10223505, "area": 450295, "currency": "Swedish krona", "url": "<a href='https://en.wikipedia.org/wiki/Sweden'>Wikipedia</a>"}, 
{"id": 2, "name": "Switzerland", "population": 8508898, "area": 41285, "currency": "Swiss franc", "url": "<a href='https://en.wikipedia.org/wiki/Switzerland'>Wikipedia</a>"}, 
{"id": 3, "name": "France", "population": 67348000, "area": 640679, "currency": "Euro", "url": "<a href='https://en.wikipedia.org/wiki/France'>Wikipedia</a>"}, 
{"id": 4, "name": "Bulgaria", "population": 7050034, "area": 110993, "currency": "Lev", "url": "<a href='https://en.wikipedia.org/wiki/Bulgaria'>Wikipedia</a>"},
{"id": 5, "name": "Belgium", "population": 11413203, "area": 30688, "currency": "Euro", "url": "<a href='https://en.wikipedia.org/wiki/Belgium'>Wikipedia</a>"}
];

const currencies = [
	{"id": 0, name: "All"},
	{"id": 1, name: "Euro"},
	{"id": 2, name: "Swedish krona"},
	{"id": 3, name: "Lev"},
	{"id": 4, name: "Swiss franc"}
]

app.get('/', (req, res) => {
    
    res.status(200);
    const currency = req.query.currency;
    if (currency && currency !== "All") {
		res.json(data.filter(c => c.currency == currency));
	} else {
		res.json(data);
	}
	res.end();
});



app.get('/currencies', (req, res) => {

    res.status(200);
    const currency = req.query.currency;
        res.json(currencies);
        res.end();
});



server.listen(port, (err) => {
	if (err) {
		throw err;
	}
});

module.exports = server;
